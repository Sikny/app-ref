package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Classe d'entree de l'application pour client programmeur :
 * import, mise a jour, suppression de services
 * @author Pamart
 *
 */
public class ClientProgrammeur {
	private final static int PORT_PROG = 3001;
	private final static String HOST = "localhost";
	
	public static void main(String[] args) {
		Socket socket = null;
		try {
			socket = new Socket(HOST, PORT_PROG);
			
			BufferedReader sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter sout = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader clavier = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("Connecté au serveur " + socket.getInetAddress() + ":" + socket.getPort());
			
			String line = new String();
			String userInput;
			
			line = sin.readLine();
			System.out.println(line.replaceAll("##", "\n"));
			
			while((userInput = clavier.readLine()) != null) {
				sout.println(userInput);
				line = sin.readLine();
				if(line != null)
					System.out.println(line.replaceAll("##", "\n"));
			}
			
			socket.close();
		} catch (IOException e) {
			System.out.println("Fin de la connexion");
		}
	}
}
