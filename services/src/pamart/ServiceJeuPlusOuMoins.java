package pamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

import bri.Service;

public class ServiceJeuPlusOuMoins implements Service{
private final Socket client;
	
	public ServiceJeuPlusOuMoins(Socket socket) {
		client = socket;
	}
@Override
	public void run() {
		try {BufferedReader in = new BufferedReader (new InputStreamReader(client.getInputStream ( )));
			PrintWriter out = new PrintWriter (client.getOutputStream ( ), true);

			Random r = new Random();
			int value = r.nextInt(101);
			
			out.println(toStringue() + "##Devinez le nombre compris entre 1 et 100 (inclus) :");
		
			int cpt = 0;
			int choice = -1;
			do {
				String line = in.readLine();
				cpt++;
				choice = Integer.parseInt(line);
				if(value > choice)
					out.println("C'est plus");
				else if(value < choice)
					out.println("C'est moins");
				else
					out.println("Bien joué, nombre trouvé en " + cpt + " coups");
			} while(value != choice);
			
			client.close();
		}
		catch (IOException e) {
			//Fin du service d'inversion
		}
	}
	
	protected void finalize() throws Throwable {
		 client.close(); 
	}

	public static String toStringue() {
		return "Jeu du plus ou moins";
	}
}
