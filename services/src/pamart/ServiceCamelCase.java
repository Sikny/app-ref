package pamart;

import java.io.*;
import java.net.*;

import bri.Service;

// rien à ajouter ici
public class ServiceCamelCase implements Service {
	
	private final Socket client;
	
	public ServiceCamelCase(Socket socket) {
		client = socket;
	}
@Override
	public void run() {
		try {BufferedReader in = new BufferedReader (new InputStreamReader(client.getInputStream ( )));
			PrintWriter out = new PrintWriter (client.getOutputStream ( ), true);

			out.println(toStringue() + "##Tapez un texte à passer au format CamelCase (traitement sur les espaces) :");
		
			String line = in.readLine();		
	
			String camelResult = new String();
			
			String[] splitted = line.split(" ");
			for(String s : splitted) {
				camelResult += s.toUpperCase().charAt(0) + s.substring(1);
			}
			
			out.println(camelResult);
			
			client.close();
		}
		catch (IOException e) {
			//Fin du service d'inversion
		}
	}
	
	protected void finalize() throws Throwable {
		 client.close(); 
	}

	public static String toStringue() {
		return "Transformation de texte au format CamelCase";
	}
}
