package services;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Classe de service de serveur, permettant la connexion d'un
 * client
 * @author Pamart
 *
 */
public abstract class ServerService implements Runnable{
	protected Socket socket;
	protected BufferedReader in;
	protected PrintWriter out;
	
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	public void connexionClient() {
		System.out.println("Client " + nomService() + " connecté, adresse : "
				+ socket.getRemoteSocketAddress().toString());
	}
	
	protected void finalize() throws Throwable {
		socket.close(); 
	}
	
	public abstract String nomService();
}
