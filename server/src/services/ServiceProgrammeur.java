package services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLClassLoader;

import bri.ServiceRegistry;
import users.Programmeur;
import users.Users;

/**
 * Service de connexion d'un programmeur
 * @author Pamart
 *
 */
public class ServiceProgrammeur extends ServerService {
	Programmeur programmeur;
	URLClassLoader urlcl;
	
	@Override
	public String nomService() {
		return "Programmeur";
	}
	
	@Override
	public void run() {
		connexionClient();
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			
			out.println("Connexion en tant que programmeur :##"
				+ "\t 1. Nouveau programmeur##"
				+ "\t 2. Déjà enregistré");
			String type = in.readLine();
			int typeInt = 0;
			try {
				typeInt = Integer.parseInt(type);
			} catch(NumberFormatException e) {
				out.println("Erreur : valeur non numérique");
				socket.close();
			}
			if(typeInt == 1) {
				nouveauProgrammeur();
			} else if(typeInt == 2) {
				connectProgrammeur();
			} else {
				out.println("Valeur non attendue");
				socket.close();
			}
			
			do {
				out.println("Connecté en tant que " + programmeur.getLogin()
						+ "##Que voulez-vous faire ?##"
						+ "1. Importer un service##"
						+ "2. Mettre à jour un service existant##"
						+ "3. Changer d'adresse ftp##"
						+ "4. Démarrer un service##"
						+ "5. Arrêter un service##"
						+ "6. Désinstaller un service##"
						+ "7. Quitter");
				try {
					typeInt = Integer.parseInt(in.readLine());
				} catch(NumberFormatException e) {
					out.println("Erreur : valeur non numérique");
					socket.close();
				}
				switch(typeInt) {
				case 1:
					urlcl = new URLClassLoader(new URL[] {new URL(programmeur.getFtp())});
					String classNameAdd = "";
					while(!isClassNameValid(classNameAdd)) {
						out.println("Entrer le nom de la classe à ajouter##"
								+ "Cette classe doit être dans un package du nom de votre login :##"
								+ programmeur.getLogin() + ".ClassName");
						classNameAdd = in.readLine();
					}
					try {
						ServiceRegistry.addService(urlcl.loadClass(classNameAdd));
						out.println("Service ajouté, appuyez sur entrée");
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						out.println("Classe non trouvée, appuyez sur entrée pour continuer");
					}
					urlcl.close();
					in.readLine();
					break;
				case 2:
					urlcl = new URLClassLoader(new URL[] {new URL(programmeur.getFtp())});
					String classNameUpd = "";
					while(!isClassNameValid(classNameUpd)) {
						out.println("Entrer le nom de la classe à mettre à jour##"
								+ "Cette classe doit être dans un package du nom de votre login :##"
								+ programmeur.getLogin() + ".ClassName");
						classNameUpd = in.readLine();
					}
					try {
						ServiceRegistry.updateService(urlcl.loadClass(classNameUpd));
						out.println("Service mis à jour, appuyez sur entrée");
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						out.println("Classe non trouvée, appuyez sur entrée pour continuer");
					} catch (IllegalArgumentException e) {
						out.println("Impossible de mettre le service à jour, est-il présent ?"
								+ "##Appuyez sur entrée pour continuer");
					}
					urlcl.close();
					in.readLine();
					break;
				case 3:
					out.println("Nouvelle adresse FTP :");
					programmeur.setFtp(in.readLine());
					out.println("Adresse FTP changée, appuyez sur entrée");
					in.readLine();
					break;
				case 4:
					String stoppedServices = ServiceRegistry.stoppedString();
					if(stoppedServices.isEmpty()) {
						out.println("Tous les services enregistrés sont démarrés##Appuyez sur entrée...");
					} else {
						out.println("Entrer le numéro du service à démarrer :##" + stoppedServices);
						int numService = Integer.parseInt(in.readLine());
						out.println(ServiceRegistry.setRunning(numService-1, true) + "Appuyez sur entrée...");
					}
					in.readLine();
					break;
				case 5:
					String runningServices = ServiceRegistry.runningString();
					if(runningServices.isEmpty()) {
						out.println("Tous les services enregistrés sont arrêtés##Appuyez sur entrée...");
					} else {
						out.println("Entrer le numéro du service à arrêter :##" + runningServices);
						int numService = Integer.parseInt(in.readLine());
						out.println(ServiceRegistry.setRunning(numService-1, false) + "Appuyez sur entrée...");
					}
					in.readLine();
					break;
				case 6:
					out.println(ServiceRegistry.toStringue() + "Tapez le numéro de service à désinstaller##");
					int numService = Integer.parseInt(in.readLine());
					ServiceRegistry.uninstallService(numService-1);
					out.println("Service désinstallé##Appuyez sur entrée...");
					in.readLine();
					break;
				case 7:
					out.println("Appuyez sur entrée...");
					in.readLine();
					socket.close();
					break;
				default:
					out.println("Valeur non attendue");
					in.readLine();
					socket.close();
				}
			} while(!socket.isClosed());
			
		} catch (IOException e) {
			System.out.println("Client " + socket.getRemoteSocketAddress().toString() + " déconnecté");
		}
	}

	/**
	 * Connecte un programmeur déjà inscrit en lui demandant son login et son mot de passe
	 * @throws IOException
	 */
	private void connectProgrammeur() throws IOException{
		out.println("Se connecter avec un login existant :##"
				+ "Entrez votre login :");
		do {
			
			String login = in.readLine();
			out.println("Entrez votre mot de passe :");
			String pass = in.readLine();
			programmeur = Users.getInstance().getProgrammeur(login, pass);
			if(programmeur == null) {
				out.println("Login ou mot de passe invalide##"
						+ "Entrez votre login :");
			}
		} while(programmeur == null);
	}
	
	/**
	 * Connecte un nouveau programmeur en l'ajoutant à la liste des programmeurs enregistrés
	 * @throws IOException
	 */
	private void nouveauProgrammeur() throws IOException{
		out.println("S'enregistrer en tant que nouveau programmeur :##"
				+ "Choisissez votre login :");
		do {
			String login = in.readLine();
			out.println("Choisissez votre mot de passe :");
			String pass = in.readLine();
			programmeur = new Programmeur(login, pass);
			if(Users.getInstance().isRegistered(programmeur)) {
				out.println("Un programmeur est déjà enregistré en tant que " + login
						+ "##Essayez avec un login différent :");
			}
		} while(Users.getInstance().isRegistered(programmeur));
		out.println("Veuillez renseigner l'url de votre serveur FTP :");
		String ftp = in.readLine();
		programmeur.setFtp(ftp);
		Users.getInstance().addProgrammeur(programmeur);
	}

	/**
	 * Renvoie true si le nom de la classe entré est valide (login.nomdeclasse)
	 * @param className
	 * @return
	 */
	private boolean isClassNameValid(String className) {
		String[] splitted = className.split("\\.");	// protection du '.'
		if(splitted.length > 0 && splitted[0].equals(programmeur.getLogin()))
			return true;
		return false;
	}
}
