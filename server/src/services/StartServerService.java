package services;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Classe permettant le chargement d'un service de connexion
 * @author Pamart
 *
 */
public class StartServerService implements Runnable{
	private ServerSocket serverSocket;
	private ServerService service;

	public StartServerService(ServerSocket sock, ServerService service) {
		this.serverSocket = sock;
		this.service = service;
	}

	@Override
	public void run() {
		while(true) {
			try {
				Socket sock = serverSocket.accept();
				service.setSocket(sock);
				new Thread(service).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
