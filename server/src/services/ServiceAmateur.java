package services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;

import bri.Service;
import bri.ServiceRegistry;

public class ServiceAmateur extends ServerService {

	@Override
	public void run() {
		connexionClient();
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			
			out.println("Connexion en tant qu'amateur##"
					+ "Tapez le numéro de service désiré :##" 
					+ ServiceRegistry.runningString());
			String serviceStr = in.readLine();
			int serviceNum = 0;
			try {
				serviceNum = Integer.parseInt(serviceStr);
			} catch(NumberFormatException e) {
				out.println("Erreur : valeur non numérique");
				socket.close();
			}
			Service service = null;
			try {
				if(ServiceRegistry.isRunning(serviceNum-1))
					service = (Service) ServiceRegistry.getServiceClass(serviceNum).getConstructor(Socket.class).newInstance(socket);
				else {
					out.println("Le service sélectionné n'est pas démarré");
					in.readLine();
					socket.close();
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
				out.println("Erreur lors du chargement du service");
				socket.close();
			}
			if(service != null) {
				System.out.println("Service " + service.getClass().getName() + " chargé");
				service.run();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String nomService() {
		return "Amateur";
	}

}
