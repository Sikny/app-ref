package users;

import java.util.List;
import java.util.Vector;

public class Users {
	static {
		instance = new Users();
	}
	private static Users instance;
	
	private List<Programmeur> programmeurData;
	
	private Users() {
		programmeurData = new Vector<Programmeur>();
	}
	
	public static Users getInstance(){
		return instance;
	}
	
	public void addProgrammeur(Programmeur p) {
		programmeurData.add(p);
	}
	
	public boolean isRegistered(Programmeur p) {
		for(Programmeur prog : programmeurData) {
			if(prog.getLogin().equals(p.getLogin()))
				return true;
		}
		return false;
	}
	
	public Programmeur getProgrammeur(String login, String pass) {
		for(Programmeur prog : programmeurData) {
			if(prog.getLogin().equals(login) && prog.getPass().equals(pass))
				return prog;
		}
		return null;
	}
	
	@Override
	public String toString() {
		String str = new String();
		for(Programmeur prog : programmeurData) {
			str += prog.getLogin() + prog.getPass();
		}
		return str;
	}
}
