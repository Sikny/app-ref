package users;

public class Programmeur {
	private String login, pass;
	private String ftpUrl;
	
	public Programmeur(String login, String pass) {
		this.login = login;
		this.pass = pass;
	}
	
	public boolean equals(Object prog) {
		Programmeur p = (Programmeur) prog;
		return p.login.equals(login) && p.pass.equals(pass);
	}
	
	public void setFtp(String url) {
		this.ftpUrl = url;
	}
	
	public String getFtp() {
		return ftpUrl;
	}

	public String getPass() {
		return pass;
	}

	public String getLogin() {
		return login;
	}
}
