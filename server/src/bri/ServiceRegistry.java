package bri;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServiceRegistry {
	// cette classe est un registre de services
	// partagée en concurrence par les clients et les "ajouteurs" de services,
	// un Vector pour cette gestion est pratique

	static {
		servicesClasses = Collections.synchronizedList(new ArrayList<Class<?>>());
		runnedServices = Collections.synchronizedList(new ArrayList<Boolean>());
	}
	private static List<Class<?>> servicesClasses;
	private static List<Boolean> runnedServices;	// services démarrés à true
	
	public static void updateService(Class<?> service) {
		if(isServiceValid(service)) {
			for(int i = 0; i < servicesClasses.size(); i++) {
				if(servicesClasses.get(i).getName().equals(service.getName())) {
					servicesClasses.set(i, service);
					System.out.println("Service mis à jour");
				}
			}
		}
	}

	// ajoute une classe de service après contrôle de la norme BLTi
	public static void addService(Class<?> service) {
		// vérifier la conformité par introspection
		if(isServiceValid(service)) {
			// si conforme, ajout au vector
			servicesClasses.add(service);
			runnedServices.add(true);
			System.out.println("Service ajouté et démarré");
		}
	}
	
	private static boolean isServiceValid(Class<?> service) {
		boolean isService = false;
		for(Class<?> i : service.getInterfaces()) {
			if(i.getName().equals(Service.class.getName())) {
				isService = true;
			}
		}

		try {
			service.getConstructor(Socket.class);
		} catch(NoSuchMethodException e) {
			e.printStackTrace();
		}
		
		boolean hasSocket = false;
		for(Field f : service.getDeclaredFields()) {
			if(f.getType().equals(Socket.class)
				&& Modifier.isPrivate(f.getModifiers()) && Modifier.isFinal(f.getModifiers())) {
				hasSocket = true;
			}
		}
		
		boolean hasToStringue = false;
		try {
			Method m = service.getMethod("toStringue");
			if(Modifier.isPublic(m.getModifiers()) && Modifier.isStatic(m.getModifiers()))
				hasToStringue = true;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		// si non conforme --> exception avec message clair
		if(!isService || Modifier.isAbstract(service.getModifiers())
			|| !Modifier.isPublic(service.getModifiers())
			|| !hasSocket || !hasToStringue) {
			throw new IllegalArgumentException("Classe non conforme :"
					+ "\nService : " + isService + "\n"
					+ "!Abstract & public : " +  (!Modifier.isAbstract(service.getModifiers()) || Modifier.isPublic(service.getModifiers()))
					+ "\nSocket : " + hasSocket
					+ "\ntoStringue : " + hasToStringue);
		} else
			return true;
	}
	
// renvoie la classe de service (numService -1)	
	public static Class<?> getServiceClass(int numService) {
		return servicesClasses.get(numService-1);
	}
	
// liste les activités présentes
	public static String toStringue() {
		String result = "Activités présentes :##";
		// todo
		int i = 1;
		for(Class<?> service : servicesClasses) {
			result += (i++) + " " + service.getName() + "##";
		}
		
		return result;
	}
	
	public static String runningString(){
		String str = new String();
		for(int i = 0; i < servicesClasses.size(); i++) {
			if(runnedServices.get(i)) {
				str += (i+1) + " " + servicesClasses.get(i).getName() + "##";
			}
		}
		return str;
	}
	
	public static String stoppedString(){
		String str = new String();
		for(int i = 0; i < servicesClasses.size(); i++) {
			if(!runnedServices.get(i)) {
				str += (i+1) + " " + servicesClasses.get(i).getName() + "##";
			}
		}
		return str;
	}
	
	public static String setRunning(int index, boolean state) {
		String result = new String();
		String stateStr = state?"démarré":"arrêté";
		if(runnedServices.get(index) == state)
			result = "Le service est déjà " + stateStr + "##";
		else result = "Service " + servicesClasses.get(index).getName() + " " + stateStr + "##";
		runnedServices.set(index, state);
		return result;
	}
	
	public static boolean isRunning(int index) {
		return runnedServices.get(index);
	}
	
	public static void uninstallService(int index) {
		Class<?> service = servicesClasses.get(index);
		servicesClasses.remove(index);
		runnedServices.remove(index);
		System.out.println("Service " + service.getName() + " désinstallé");
	}
}
