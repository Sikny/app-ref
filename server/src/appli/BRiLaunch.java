package appli;

import java.io.IOException;
import java.net.ServerSocket;

import services.ServiceAmateur;
import services.ServiceProgrammeur;
import services.StartServerService;

/**
 * Classe de demarrage du serveur
 * @author Pamart
 *
 */
public class BRiLaunch {
	private final static int PORT_AMA = 3000, PORT_PROG = 3001;
	
	public static void main(String[] args) {
		ServerSocket servSocketAma;
		try {
			servSocketAma = new ServerSocket(PORT_AMA);
			ServerSocket servSocketProg = new ServerSocket(PORT_PROG);
			
			new Thread(new StartServerService(servSocketAma, 
					new ServiceAmateur())).start();
			new Thread(new StartServerService(servSocketProg, 
					new ServiceProgrammeur())).start();
			
			System.out.println("Serveur démarré sur les ports " + PORT_AMA + " et " + PORT_PROG);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
